(de trie>new ()
   (cons NIL (box)) )

(de trie>insert (Trie V)
   (let Ltrs (chop V)
      (do-trie-insert (cdr Trie) Ltrs) ) )

(de do-trie-insert (Node Ltrs)
   (if Ltrs
      (let (Ltr (car Ltrs) Rst (cdr Ltrs))
         (ifn (lup (val Node) Ltr)
            (let Child (cons Ltr (box))
               (idx Node Child T)
               (do-trie-insert (cdr Child) Rst) )
            (do-trie-insert (cdr @) Rst) ) ) ) )

(de trie>check (Trie V)
   (let Ltrs (chop V)
      (do-trie-check (cdr Trie) Ltrs) ) )

(de do-trie-check (Node Ltrs)
   (ifn Ltrs
      T
      (let (Ltr (car Ltrs) Rst (cdr Ltrs))
         (ifn (lup (val Node) Ltr)
            NIL
            (do-trie-check (cdr @) Rst) ) ) ) )


# testing
# (setq T1 (trie>new))
# (trie>insert T1 "abc")
# (trie>insert T1 "abd")
# (trie>insert T1 "bc")
# (trie>insert T1 "ad")

# (println (trie>check T1 "abd")) # T
# (println (trie>check T1 "abc")) # T
# (println (trie>check T1 "bc")) # T
# (println (trie>check T1 "ad")) # T
# (println (trie>check T1 "ab")) # T
# (println (trie>check T1 "ac")) # NIL
# (println (trie>check T1 "bd")) # NIL
# (println (trie>check T1 "c")) # NIL